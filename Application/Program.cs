﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Application
{
    class Solution
    {
        public static int simpleArraySum(int[] ar) => ar.Sum();

        public static List<int> compareTriplets(List<int> a, List<int> b)
        {
            List<int> c = new List<int>() { 0, 0};
            for(int i = 0; i < a.Count; i++)
                if (a[i] > b[i])
                    c[0]++;
                else if (b[i] > a[i])
                    c[1]++;
            return c;
        }

        public static void miniMaxSum(int[] arr)
        {
            Array.Sort(arr);
            double a = 0, b = 0;
            for (int i = 0; i < (arr.Length - 1); i++)
                a += arr[i];
            for (int i = (arr.Length - 1); i > 0; i--)
                b += arr[i];
            Write(a + " " + b);
        }

        public static void plusMinus(int[] arr)
        {
            double p = 0, n = 0, z = 0;
            if(arr.Length > 0)
                for (int i = 0; i < arr.Length; i++)
                    switch (arr[i].CompareTo(0)) { case 1: p++; break; case 0: z++; break; case -1: n++; break; }
            WriteLine((p/arr.Length) + "\n" + (n/arr.Length) + "\n" + (z/arr.Length));
        }

        public static void staircase(int n)
        {
            int m = n;
            for (int i = 0; i < n; i++)
            {
                for(int j = 1; j <= n; j++)
                    if (j < m)
                        Write(" ");
                    else
                        Write("#");
                m--;
                WriteLine();
            }
        }

        public static int birthdayCakeCandles(int[] ar)
        {
            var max = ar[0];
            var ocurrencia = 1;

            for (int i = 1; i < ar.Length; i++)
            {
                if (ar[i] == max)
                {
                    ocurrencia++;
                    continue;
                }
                if (ar[i] > max)
                {
                    max = ar[i];
                    ocurrencia = 1;
                }
            }
            return ocurrencia;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Write("Menú: ");
            byte x = byte.Parse(ReadLine());
            switch (x)
            {
                case 1:
                    int[] array = new int[] { 1, 5, 6, 2 };
                    WriteLine(Solution.simpleArraySum(array));
                    break;
                case 2:
                    List<int> a = new List<int>() { 5, 6, 7 };
                    List<int> b = new List<int>() { 3, 6, 10 };
                    WriteLine(Solution.compareTriplets(a, b));
                    break;
                case 3:
                    array = new int[] { 5, 5, 5, 5, 5 };
                    Solution.miniMaxSum(array);
                    break;
                case 4:
                    array = new int[] { 0 };
                    Solution.plusMinus(array);
                    break;
                case 5:
                    Solution.staircase(int.Parse(ReadLine()));
                    break;
                case 6:
                    array = new int[] { 3, 2, 1, 3 };
                    WriteLine(Solution.birthdayCakeCandles(array));
                    break;
            }
            ReadKey();
        }
    }
}
